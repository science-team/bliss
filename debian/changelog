bliss (0.77-3) unstable; urgency=medium

  * Debianization:
    - d/rules:
      - override_dh_auto_install-indep target, cleanup.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Jul 2023 22:05:07 +0000

bliss (0.77-2) unstable; urgency=medium

  * Debianization:
    - d/rules:
      - override_dh_auto_install-indep target, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Jul 2023 20:18:29 +0000

bliss (0.77-1) unstable; urgency=medium

  * New minor upstream version(Closes: #1032936).
  * Debianization:
    - d/watch:
      - URL, add trailing slash;
    - d/patches/*
      - d/upstrean-fix-apparent_forgotten_assert-commented.patch, introduce;
      - d/p/upstream-examples-enumerate-warnings-silence.patch, introduce;
      - d/p/upstream-source-*.patch, refresh;
      - d/p/upstream-autotoolization.patch, reorganize;
      - d/p/debianization.patch, update;
    - d/adhoc/examples/*:
      - d/a/e/Makefile, refresh;
      - d/a/e/enumerate.output, introduce;
    - d/README.Debian, update;
    - d/libbliss2.lintian-overrides:
      - exit-in-shared-library override, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 09 Jul 2023 17:16:37 +0000

bliss (0.73-6) unstable; urgency=medium

  [ Jerome Benoit ]
  * Update copyright year-tuples in d/copyright.
  * Bump Standards Version to 4.6.2 (no change).
  * Update upstream website address.
  * Various janitorial changes.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + libbliss-dev-common: Drop versioned constraint on libbliss-dev in
      Replaces.
    + libbliss-dev-common: Drop versioned constraint on libbliss-dev in Breaks.
    + Remove 2 maintscript entries from 2 files.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 01 Jul 2023 20:37:20 +0000

bliss (0.73-5) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - copyright year tuples, update;
    - debian/control:
      - Maintainer address, update;
      - Rules-Requires-Root, introduce and set to no;
      - Standards Version, bump to 4.5.1 (no change);
      - Build-Depends, migrate to debhelper-compat (=13) (major changes);
      - libbliss-dev depends on libgmp-dev version 2:6.2.0 or higher
          (pkg-config support);
    - debian/rules:
      - override_dh_installdocs target, discard (dh migration);
      - override_dh_missing target, idem;
    - debian/*.lintian-overrides:
      - d/{bliss-doc,libbliss-dev-common}.l-o, discard (update routine);
      - d/libbliss2.l-o, refreah (idem);
    - debian/{bliss,libbliss-dev}.maintscript, update (dh migration);
    - debian/compat, discard (debhelper migration).
    - debia/tests/*:
      - d/t/build-examples:
        - replace ADTTMP with AUTOPKGTEST_TMP;
        - make invocation, add -k option;
        - paths, refresh (debhelper migration);
      - build-examples Tests Depends field, add pkg-config;
    - debian/patches/*:
      - d/p/upstream-autotoolization.patch, add pkg-config support;
    - debian/libbliss-dev.install, install now pkg-config metadat file;
    - debian/adhoc/examples/*:
      - d/a/e/myciel3-col.output, introduce;
      - d/a/e/Makefile, harden;
    - debian/upstream/metadata, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 25 Dec 2020 15:02:34 +0000

bliss (0.73-4) unstable; urgency=medium

  * Upload to unstable.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.4.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 19 Jul 2019 12:48:44 +0000

bliss (0.73-3) experimental; urgency=medium

  * Debianization:
    - debian/copyright:
      - copyright tuples, update;
      - Files field:
        - d/a/e/myciel3.col, introduce;
    - debian/control:
      - Standards Version, bump to 4.3.0 (no change);
    - debian/adhoc/examples/*:
      - d/a/e/Makefile:
        - copyright tuple, update;
        - check target, harden (Closes: #923527), thanks to
          Bernhard Übelacker <bernhardu@mailbox.org>;
      - d/a/e/myciel3.col, introduce;
    - debian/tests/control, harden;
    - debian/source/lintian-overrides:
      - debian-watch-uses-insecure-uri tag, add comment.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 30 Mar 2019 09:18:38 +0000

bliss (0.73-2) unstable; urgency=medium

  [ Jerome Benoit ]
  * d/control: Standards-Version bump to 4.2.1 (no change).
  * d/control: Vcs-* fields, migration to salsa.
  * d/copyright: Copyright fields, update.
  * d/rules: include pkg-info.mk (simplification).
  * d/adhoc/Makefile: refresh.
  * d/s/lintian-overrides, refresh.
  * d/p/upstream-autotoolization.patch, fix typo.

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 14 Nov 2018 10:14:04 +0000

bliss (0.73-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #818966).
  * Debianization:
    - debian/copyright:
      - bump format to DEP-5;
      - refresh;
    - debian/control:
      - discard the debug symbols package libbliss1d-dbg;
      - Standards Version, bump to 3.9.8;
      - Vcs-* headers, secure;
    - debian/watch, refresh;
    - debian/rules:
      - default target, introduce;
      - get-orig-source target, introduce;
      - discard the debug symbols material (see above);
      - build-arch/build-indep scheme, introduce;
      - clean up;
    - debian/tests; initiate;
    - reproducible build, attempt.
  * Upstream fixes and enhancements has been submitted and defining
    a clear ABI has been suggested to the upstream maintainer.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 25 Apr 2016 02:23:27 +0000

bliss (0.72-5) unstable; urgency=low

  * Convert packaging to 3.0 (quilt).
  * Bug fix: "FTBFS with clang instead of gcc", thanks to Sylvestre Ledru
    (Closes: #710399). Replace -O9 with -O4.
  * Add Vcs-Git and Vcs-Browser headers.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Bump debhelper compat level to 9
  * Enable hardening flags

 -- David Bremner <bremner@debian.org>  Fri, 07 Jun 2013 08:22:00 -0300

bliss (0.72-4) unstable; urgency=low

  * Force BLISS_USE_GMP to be defined so that user compiled code matches
    the library.

 -- David Bremner <bremner@debian.org>  Mon, 05 Nov 2012 20:14:20 -0400

bliss (0.72-3) unstable; urgency=low

  * Link libbliss.so.1d.0 against gmp (Closes: #685390).

 -- David Bremner <bremner@debian.org>  Mon, 20 Aug 2012 17:28:30 +0200

bliss (0.72-2) unstable; urgency=low

  * Add debugging libraries.
  * Actually build with GMP, as promised in README.Debian.

 -- David Bremner <bremner@debian.org>  Sun, 20 Nov 2011 11:41:31 -0400

bliss (0.72-1) unstable; urgency=low

  * Fix changelog bug number typo
  * New upstream version
  * Bump standards version to 3.9.2 (no changes)
  * Update package description, thanks to Martin Eberhard
    Schauer and Justin B Rye (Closes: #634057).

 -- David Bremner <bremner@debian.org>  Sun, 28 Aug 2011 00:58:23 -0300

bliss (0.71-2) unstable; urgency=low

  * Remove symbols file (Closes: #626028)

 -- David Bremner <bremner@debian.org>  Sun, 08 May 2011 06:38:42 -0300

bliss (0.71-1) unstable; urgency=low

  * New upstream release
  * Replace ligatures in description (Closes: #613480)
  * Change section of libbliss0d to libs (Closes: #614606)

 -- David Bremner <bremner@debian.org>  Thu, 05 May 2011 19:11:51 -0300

bliss (0.50-1.1) unstable; urgency=low

  * NMU.  Build with unversioned libgmp-dev.

 -- Steve M. Robbins <smr@debian.org>  Tue, 15 Mar 2011 01:46:08 -0500

bliss (0.50-1) unstable; urgency=low

  * Initial release (Closes: #528925)

 -- David Bremner <bremner@debian.org>  Thu, 16 Dec 2010 23:34:03 -0400
